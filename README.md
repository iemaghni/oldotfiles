# Dotfiles

### QEMU
```sh
# `-M q35` causes segfault on linux mint
# -bios /usr/share/qemu/OVMF.fd
# -redir tcp:8080::8080
# -drive "if=pflash,format=raw,file=/media/media/vm/OVMF-pure-efi.fd,readonly"
# -device ahci,id=ahci -device ide-drive,drive=disk,bus=ahci.0 -drive id=disk,if=none,file=<PATH>
qemu-img create raw.img 4G
qemu-system-x86_64 -cpu host -smp 4 -m 4G -vga virtio \
 -machine type=q35,accel=kvm,usb=off,vmport=off -usb -device usb-tablet \
 -drive if=pflash,readonly,format=raw,file=/usr/share/ovmf/x64/OVMF_CODE.fd \
 \
 -device virtio-scsi-pci,id=scsi \
 -device scsi-hd,drive=hd,bus=scsi.0 \
 -drive if=none,aio=native,cache=none,discard=unmap,format=raw,file=raw.img,id=hd \
 \
 -device virtio-net,netdev=vmnic \
 -netdev user,id=vmnic \
 \
 -device virtio-serial \
 -chardev socket,path=/tmp/qga.sock,server,nowait,id=qga0 \
 -device virtserialport,chardev=qga0,name=org.qemu.guest_agent.0 \
 \
 -device e1000,netdev=pforw \
 -netdev user,hostfwd=tcp::8080-:8080,id=pforw \
 -cdrom <ISO_FILE>
```

### Instructions
```sh
# login as root:voidlinux
bash # for completion
xbps-install -Syu
xbps-install gptfdisk
cd /dev
cgdisk vda
# SEE BELOW FOR UEFI
mkfs.vfat vda1 -F32 # esp
mkfs.ext4 vda2
mount vda2 /mnt
cd $_
mkdir boot
mount /dev/vda1 $_
xbps-install -SR https://repo.voidlinux.eu/current -r. base-system git
mount -t proc proc proc
mount -t sysfs sys sys
mount -o bind /dev dev
mount -t devpts pts dev/pts
cp /etc/resolv.conf etc
chroot .
useradd vm -s/bin/sh
passwd vm
passwd
rmdir var/tmp
cd home/vm
git clone git://github.com/concatime/dotfiles
cp -r dotfiles/workstation/. /
chown vm:vm -R .
cd -
dracut --force --hostonly '' `ls lib/modules`
xbps-reconfigure -f glibc-locales linux4.17
exit
cd ..
umount -R mnt
```

### Run
```sh
dotfiles/run
sudo rsync -lr dotfiles/workstation/ /
sudo chown -R vm:vm .
```

### Edit
```
# /etc/pam.d/system-local-login
session optional pam_rundir.so
```

### UEFI
ESP partition (`ef00`) should be formated as fat16 or fat32 (`mkfs.vfat -F32`). If your rely on [EFISTUB](//wiki.archlinux.org/index.php/EFISTUB), then `esp` partition should be mounted as `/boot` since UEFI needs direct access (can only read fat), with a prefered size of atleast `128m`. You can then use `efibootmgr` to manage your boot entries.

Note that on some UEFI implementations (e.g. Dell), you cannot pass arguments to the kernel. You also [cannot](//wiki.gentoo.org/wiki/Talk:EFI_stub_kernel#Initramfs) provide `initrd` argument in `CONFIG_CMDLINE`. You have two choices in this case, either use a bootloader in-between (e.g. systemd-boot, grub, rEFInd) or build linux kernel with [`CONFIG_INITRAMFS_SOURCE`](//landley.net/writing/rootfs-howto.html). If you do use the last option, consider mounting `esp` as `/boot/efi` and copy your `vmlinuz` into `efi/boot/bootx64.efi` (fat is case insensitive). This way, your UEFI bootloader will pick it automatically.

I will use `startup.nsh` instead of `efiboomgr` for the sake of simplicity, and also because VirtualBox does not retain EFI variables between cold boots. Otherwise, you can install grub and mount `esp` as either `/boot` or `/boot/efi` (default) with a size of `256m`.
```sh
xbps-install […] grub-x86_64-efi
grub-install --recheck --debug --verbose #--efi-directory=boot
# Default path is <esp>/EFI/BOOT/BOOTX64.EFI
```

### Groups
List of groups: `audio` `video` `wheel` `tty` `kvm`

### Keyboard
In /usr/share/X11/xkb/rules/evdev', right after `! options<tab>=<tab>symbols`:
`custom:swap_colon_apostrophe = +custom(swap_colon_apostrophe)`

### Wallpapers
[Marilyn Monroe](//stanbos.deviantart.com/art/Marilyn-Monroe-Pooh-pooh-bee-doo-WIP-351117726) by Stanbos

[Daft Punk](//www.deviantart.com/ronmustdie/art/Daft-Punk-295414356) by runmustdie

[Sunset](//www.deviantart.com/axiomdesign/art/Neon-Sunset-4k-709421733)

[Firefox](//www.deviantart.com/marilucia/art/Firefox-446171818)

[Fumikage Tokoyami](//www.deviantart.com/slezzy7/art/Fumikage-Tokoyami-BnHA-Minimalist-Wallpaper-699515780)

[Weather Deity](//www.deviantart.com/art/Weather-Deity-743892889) by Remarin

[Landscape](//www.artstation.com/artwork/Y2Wew) by Mikael Gustafsson

[Field](//www.artstation.com/artwork/QxLLr) by Jakub Rozalski

[Mountains](//pureos.net/downloads/art/wallpapers/mountains4.png) by PureOS

Firewatch
