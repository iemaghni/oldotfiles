# DELL Inspiron 7567

## BIOS

### Issue
All versions after `1.1` cause random reboots. Dell even removed the `1.4.0` version which blocks BIOS downgrades.

### Revert
If you flashed the 1.4.0 version, you're that out of lock. As you saw, you cannot downgrade using the installer. There is another way to revert. [Steps](//www.dell.com/community/General/Dell-Inspiron-15-7000-is-turning-off-and-restarting-after-BIOS/m-p/5168691/highlight/true#M933874).

### Solution
After some investigations, it seems that Dell’s engineers [will patch](//www.dell.com/community/Inspiron/Dell-7567-BIOS-1-5-3-still-is-turning-off-and-restarting/m-p/6048522/highlight/true#M17190) soon this issue.
[//]: # (dell.com/community/General/Dell-Inspiron-15-7000-is-turning-off-and-restarting-after-BIOS/m-p/5651516/highlight/true#M941460)

## Errors (`journalctl --boot --priority err`)

### ~~BBSwitch~~
Issue: `bumblebeed[…]: Module 'bbswitch' not found.`

Solution: [`hardware.bumblebee.pmMethod = "bbswitch";`](//github.com/NixOS/nixpkgs/issues/24711#issuecomment-357791636).

### snd_pcm_oss
Issue: `systemd-modules-load[…]: Failed to find module 'snd_pcm_oss'`
```
[16:13] <concatime> Hi. Does someone know how to solve this message `Failed to find module 'snd_pcm_oss'`?
[16:24] <boxofrox> concatime: what kernel version are you on?  `uname -r`
[16:25] <concatime> Latest, 4.14.13
[16:29] <boxofrox> concatime: you might check the modules for your kernel to verify snd-pcm-oss.ko is there.  `find /nix/store -max-depth 1 -name '*linux-4.14.13*'` will show you the path to your kernel files.  then `find /nix/store/KERNEL_PATH/lib/modules/4.14.13/kernel -name 'snd-pcm-oss*'` will verify the module exists.
[16:30] <concatime> s/max-depth/maxdepth
[16:31] <boxofrox> concatime: oops, sorry about that
[16:32] <concatime> find /nix/store/j56bhs6zfcma37f5zdrb6fiq1dbna4wq-linux-4.14.13/lib/modules/4.14.13/kernel -name 'snd-pcm-oss*' gave me nothing.
[16:33] <concatime> boxofrox: Nothing found
[16:34] <boxofrox> concatime: seems that package didn't compile snd-pcm-oss as a module.  the driver might be compiled into the kernel, though I'm not sure how to confirm that.  I'm using linux-4.9.76 on one of my servers and snd-pcm-oss.ko exists.
[16:36] <concatime> boxofrox: Maybe I need to add it into availableKernelModules?
[16:37] <boxofrox> concatime: my server doesn't specify that module in availableKernelModules, but the module is loaded.  :/
[16:37] <concatime> I have (almost) everything inside https://github.com/concatime/dotfiles/blob/master/nixos/laptop.nix
[16:38] <concatime> I tried to add `"snd_pcm_oss"`, in vain.
[16:39] <boxofrox> that would just copy the module into the initrd, I believe.  you're still missing the file.
[16:51] <boxofrox> concatime: best I can figure, 4.14 doesn't compile the snd-pcm-oss module.  visit https://hydra.nixos.org/build/63981722/nixlog/3 and search for 'snd-pcm-oss' and you'll see the module for my kernel is built.  for linux-latest (4.14) https://hydra.nixos.org/build/63990222/log doesn't show the module was compiled.
[16:51] <boxofrox> concatime: 4.13 lacks the snd-pcm-oss module, too.
[16:55] <concatime> boxofrox: You're totally right! There is no `snd-pcm-oss`. Is this intended by NixOS, or an issue?
[16:57] <boxofrox> concatime: i'd suspect the linux devs either removed the driver or disabled it by default.  nothing in nixos/nixpkgs pops out at me that nixos specifies which modules to build.
[16:57] <concatime> wilornel: `security.sudo.wheelNeedsPassword = false;`
[16:57] <concatime> Then sudo is not a problem anymore ;)
[16:58] <MP2E> I'm guessing unintentional since there's no reference to snd-pcm-oss in the git log, or OSS in the context of our kernels
[16:58] <MP2E> yeah nixpkgs doesn't seem to have any changes related to disabling it or anything
[16:58] <MP2E> hm
[17:04] <boxofrox> concatime: oss was dropped from linux.  https://github.com/torvalds/linux/commit/727dede0ba8afbd8d19116d39f2ae8d19d00033d#diff-ecedcdca1f8e6c93a5b538cef26256b3
[17:06] <boxofrox> soo... to get rid of your error, you'll have to find the thing that's trying to load the snd-pcm-oss module and make it stop :)
[17:06] <concatime> boxofrox: Let's have some fun ;) /s
```

### Remaining
```
-- Logs begin at Sun 2018-01-14 19:09:56 EST, end at Mon 2018-01-15 16:09:06 EST. --
kernel: ACPI Error: [\_SB_.PCI0.XHC_.RHUB.HS11] Namespace lookup failure, AE_NOT_FOUND (20170728/dswload-210)
kernel: ACPI Exception: AE_NOT_FOUND, During name lookup/catalog (20170728/psobject-252)
kernel: ACPI Exception: AE_NOT_FOUND, (SSDT:xh_OEMBD) while loading table (20170728/tbxfload-228)
kernel: ACPI Error: 1 table load failures, 10 successful (20170728/tbxfload-246)
kernel: mce: [Hardware Error]: CPU 0: Machine Check: 0 Bank 6: ee0000000040110a
kernel: mce: [Hardware Error]: TSC 0 ADDR fef1ffc0 MISC 7880010086
kernel: mce: [Hardware Error]: PROCESSOR 0:906e9 TIME 1516049627 SOCKET 0 APIC 0 microcode 80
kernel: mce: [Hardware Error]: CPU 0: Machine Check: 0 Bank 7: ee0000000040110a
kernel: mce: [Hardware Error]: TSC 0 ADDR fef1ce40 MISC 47880010086
kernel: mce: [Hardware Error]: PROCESSOR 0:906e9 TIME 1516049627 SOCKET 0 APIC 0 microcode 80
kernel: mce: [Hardware Error]: CPU 0: Machine Check: 0 Bank 8: ee0000000040110a
kernel: mce: [Hardware Error]: TSC 0 ADDR fef1ff40 MISC 7880010086
kernel: mce: [Hardware Error]: PROCESSOR 0:906e9 TIME 1516049627 SOCKET 0 APIC 0 microcode 80
kernel: mce: [Hardware Error]: CPU 0: Machine Check: 0 Bank 9: ee0000000040110a
kernel: mce: [Hardware Error]: TSC 0 ADDR fef1ff00 MISC 3880010086
kernel: mce: [Hardware Error]: PROCESSOR 0:906e9 TIME 1516049627 SOCKET 0 APIC 0 microcode 80
[snd_pcm_oss]
systemd-udevd[…]: Specified group 'kvm' unknown
kernel: acpi INT3400:00: Unsupported event [0x86]
kernel: tpm_crb MSFT0101:00: [Firmware Bug]: ACPI region does not cover the entire command/response buffer. [mem 0xfed40000-0xfed4087f flags 0x200] vs fed40080 f80
kernel: tpm_crb MSFT0101:00: [Firmware Bug]: ACPI region does not cover the entire command/response buffer. [mem 0xfed40000-0xfed4087f flags 0x200] vs fed40080 f80
[BBSwitch]
kernel: [drm:lspcon_init [i915]] *ERROR* Failed to probe lspcon
kernel: [drm:intel_modeset_init [i915]] *ERROR* LSPCON init failed on port B
```

## Sway

### [Inconsistent NumLock](//github.com/swaywm/sway/issues/1466#issuecomment-347061246)
This issue should vanish with Sway 1.0 which get rid of `wlc` in favor to `wlroots`.

### [Sporadic mouuse disappearance](//github.com/swaywm/sway/issues/1127#issuecomment-349699725)
Happens when hovering over VSCode. Should be fixed with `wlroots`.

### [~~Semicolon~~](://github.com/swaywm/sway/issues/608#issuecomment-215242812)
Use `bindcode` with `;` or `bindsym` with `semicolon`.

## Neovim-Qt

### [Copy/Paste on Wayland](//github.com/equalsraf/neovim-qt/issues/327)

## MoSH

### No scroll
[_Almost_ a dealbreaker](//github.com/mobile-shell/mosh/issues/122).
