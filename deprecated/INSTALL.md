## Steps

### Minimal
```sh
cd /dev
# {} = [sda nvme0n1]
cgdisk {}
# ef00 boot {128,256}m linux-libre requires less space
# 8200 swap {6144,8192}m
# 8304 root {32768,65536}m
# 8302 home
mkfs.vfat {,p}1 #-F32
mkswap {,p}2
swapon {,p}2
mkfs.ext4 {,p}3 #-F
mkfs.xfs {,p}4  #-f
mount {,p}3 /mnt
cd $_
mkdir boot home
mount /dev/{,p}1 boot
mount /dev/{,p}4 home
pacman -Sy parabola-keyring
pacstrap . base-openrc
genfstab . >> etc/fstab
arch-chroot . bin/sh
cd etc
echo tmpfs /tmp tmpfs nodev,nosuid 0 0 >> fstab
#echo LC_CTYPE=C.UTF-8 >> locale.conf # LC_COLLATE
ln -s /usr/share/zoneinfo/America/Montreal localtime
cd -
# May be worth to glance at ‘linux-libre-pck’ in near future…
pacman -U http://archlinux.org/packages/testing/x86_64/linux-zen{,-headers}/download
# If VirtualBox, do not execute efibootmgr; instead this:
#echo vmlinuz-linux-zen rw root=dev/{,p}3 initrd=initramfs-linux-zen.img > boot/startup.nsh
CNTRL_D
cd ..
umount -R mnt
efibootmgr -d dev/{} -cL'Parabola GNU+Linux' -l vmlinuz-linux-zen -u 'rw root=dev/{,p}3 initrd=initramfs-linux-zen.img'
```

### Advanced
```sh
loadkeys cf
cd /dev
# {} = [sda nvme0n1]
dd if={zero,urandom} of={}
cgdisk {}
# ef00 /boot [256m]
# 8200 /swap [{6144,8192}m]
# 8300 (default)
# 8309 LUKS
mkfs.vfat {,p}1 #-F32
mkswap {,p}2
swapon {,p}2
cryptsetup --key-size 512 luksFormat --type luks2 {,p}3
cryptsetup luksOpen {,p}3 luks
pvcreate mapper/luks
vgcreate vg0 mapper/luks
lvcreate -L65536     vg0 -n root
lvcreate -l+100%FREE vg0 -n home
mkfs.xfs  mapper/vg0-home #-f
mkfs.ext4 mapper/vg0-root #-F
mount     mapper/vg0-root /mnt
cd /mnt
mkdir -p home    boot/efi
mount /dev/{,p}1 boot/efi
mount /dev/vg0/home home
pacman -Sy parabola-keyring
pacstrap . base-openrc
pacstrap . grub efibootmgr
pacstrap . wpa_supplicant
genfstab . >> etc/fstab
arch-chroot . #/bin/bash
echo parabola > etc/hostname
ln -s /usr/share/zoneinfo/America/Montreal etc/localtime
; /etc/locale.conf
echo KEYMAP=cf > etc/vconsole.conf
nano +52 etc/mkinitcpio.conf
# HOOKS=(… encrypt lvm2)
mkinitcpio -p linux-libre
nano +940 etc/lvm/lvm.conf
# use_lvmetab=0 AT LINE 940
nano etc/default/grub
# GRUB_CMDLINE_LINUX="cryptdevice=/dev/sda3:luks"
# GRUB_PRELOAD_MODULES="… lvm" NOT NECESSARY
# GRUB_ENABLE_CRYPTODISK=y
grub-install --target=x86_64-efi --verbose #--efi-directory=/boot --bootloader-id=grub --recheck
grub-mkconfig -o boot/grub/grub.cfg
CNTRL_D
cd ..
umount mnt/{home,boot/efi,.}
```

### NOTES
```
https://wiki.parabola.nu/Installation_Guide#Verification_of_package_signatures
startup.nsh: Add extra spaces in the beginning of the line in the file. There is a byte order mark at the beginning of the line that will squash any character next to it which will cause an error when booting.
root=/dev initrd=\init
efibootmgr: UEFI uses backward slash \ as path separator but efibootmgr automatically converts UNIX-style / path separators.
-l/vmlinuz
In fact, on parabola GNU/Linux, /etc/os-release used to be a symlink to /usr/lib/os-release, and apparently is missing from current releases, which causes the distro to be incorrectly identified as arch.
https://bbs.archlinux.org/viewtopic.php?id=232393
http://www.ondatechnology.org/wiki/index.php?title=Booting_the_Linux_Kernel_without_a_bootloader
eudev
gvfs:https://askubuntu.com/a/342209
```

### POST
```sh
useradd -m issam -G wheel -G audio -s /usr/bin/fish #-u1000
passwd issam
passwd
```

As `issam`:
```sh
git clone git://github.com/concatime/dotfiles
cd dotfiles
su -c 'cp -r root/. /'
script
```

### ISSUES
```
del_efi: https://github.com/rhboot/efibootmgr/issues/47#issuecomment-396362679
qt5-webengine: https://labs.parabola.nu/issues/1167
IntelliJ: https://labs.parabola.nu/issues/1631
```
