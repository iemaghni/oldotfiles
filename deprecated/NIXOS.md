```sh
loadkeys cf
# PLUG ETHERNET
nix-shell -p git --run 'git clone git://github.com/concatime/dotfiles'
# UNPLUG IT
cp dotfiles/.nix-channel .
cp dotfiles/wpa_supplicant.conf /etc
vi /etc/wpa_supplicant.conf # Add SSID
systemctl start wpa_supplicant # https://github.com/NixOS/nixpkgs/issues/1204
nix-channel --update
cd /dev
# /dev/{sda,nvme0n1}
dd if={zero,urandom} of={}
cgdisk {}
# ef00 /boot [256mb]
# 8200 /swap [{6144,8192}mb]
# 8300 (default) # LVM=8e00
mkfs.vfat {,p}1
#mkswap   {,p}2
cryptsetup luksFormat {,p}3 # --use-random
cryptsetup luksOpen   {,p}3 luks
pvcreate mapper/luks
vgcreate vg0 $_
lvcreate -L65536     vg0 -n root
lvcreate -l+100%FREE vg0 -n home
mkfs.xfs  mapper/vg0-home # -f
mkfs.ext4 mapper/vg0-root # -F
mount $_ /mnt
cd    $_
mkdir boot home etc
mount /dev/{,p}1    boot
mount /dev/vg0/home home
cp -R ~/dotfiles/nixos /etc/wpa_supplicant.conf etc
vi etc/nixos/configuration.nix # Comment almost all user's packages, otherwise `no space left`
nixos-install
cp /root/.nix-channels root
umount boot home
cd
umount /mnt
reboot
```
