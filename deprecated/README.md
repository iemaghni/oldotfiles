## Overwiew
### Architecture
```
                      +------+-------+
                      | Root | /home |
        +-------------+ ext4 |  xfs  |
        |    Swap     +------+-------+
+-------+-------------+     LVM      |
| /boot |  LUKS with  +--------------+
|  GRUB | random keys |     LUKS     |
+-------+-------------+--------------+
TODO: RANDOM KEY
```

As [`issam`]:
```
git clone git@github.com:concatime/dotfiles
cp -R dotfiles/.{config,local} .
sudo mkdir /mnt
yes | sudo sensors-detect
source dotfiles/nvim.fish
```

Sources: [`neovim`](//stackoverflow.com/a/12834450), [`i3status-rust`](//github.com/NixOS/nixpkgs/issues/33096#issuecomment-354036903) & [`sensors`](kernelreloaded.com/lm-sensors-deployment-issue-with-sensors-detect).

Debug ([`LVM on LUKS`](//bbs.archlinux.org/viewtopic.php?pid=1291547#p1291547)):
```sh
vgchange -ay
wpa_passphrase SSID 'PASSPHRASE' >/etc/wpa_supplicant.conf
```

### Nifty commands

[SWAP](//nixos.org/nixos/manual/options.html#opt-swapDevices._.randomEncryption.cipher): `nix-shell --packages cryptsetup --run 'cryptsetup benchmark'`

HD: `nix-shell -p hdparm --run 'sudo hdparm -s 1 --yes-i-know-what-i-am-doing /dev/sda'`

```
https://github.com/deepfire/nixos-wiki/blob/master/Encrypted%20Root%20on%20NixOS.page
https://gist.github.com/mattiaslundberg/8620837
https://gist.github.com/heppu/6e58b7a174803bc4c43da99642b6094b

Default compiled-in device cipher parameters:
        loop-AES: aes, Key 256 bits
        plain: aes-cbc-essiv:sha256, Key: 256 bits, Password hashing: ripemd160
        LUKS1: aes-xts-plain64, Key: 256 bits, LUKS header hashing: sha256, RNG: /dev/urandom
```

```
# OLD: https://shapeshed.com/linux-wifi
wpa_cli
scan
scan_results
add_network
set_network 0 ssid "SSID"
set_network 0 psk  "SECRET"
enable_network 0
save_config
```

# Wallpapers
Mountains from [PureOS](//pureos.net/downloads/art/wallpapers/mountains4.png)

Landscape by Mikael Gustafsson from [artstation](//www.artstation.com/artwork/Y2Wew)

Weather Deity by Remarin from [deviantart](//www.deviantart.com/art/Weather-Deity-743892889)
