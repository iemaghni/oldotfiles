alias .. 'cd ..'
alias xx exit
alias ff 'firefox --private-window'
alias po 'sudo poweroff'
alias re 'sudo reboot'
alias udf 'git -C ~/dotfiles pull'
alias irc 'irssi --home=~/.config/irssi'
alias gpg 'gpg2 --homedir=~/.config/gnupg' # $GNUPGHOME
alias key xev # faq.i3wm.org/question/3747/enabling-multimedia-keys.1.html
alias keys 'xmodmap -pke'
alias xfce startxfce4
alias ciphers 'nmap --script ssl-enum-ciphers -p 443' # superuser.com/a/763908
