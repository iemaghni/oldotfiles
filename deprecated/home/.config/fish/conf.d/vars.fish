#set -q XDG_RUNTIME_DIR; or \
#set -x XDG_RUNTIME_DIR /run/user/(id -u)
#set -x XKB_DEFAULT_LAYOUT us #ca
#set -x GDK_VULKAN_DEVICE list # phoronix.com/scan.php?page=news_item&px=GTK-Vulkan-Pick-The-GPU
#set -x GDK_BACKEND     wayland # Firefox/Chromium crashing!
#set -x SDL_VIDEODRIVER wayland
#set -x CLUTTER_BACKEND wayland
#set -x QT_QPA_PLATFORM wayland # window zoomed
# https://wiki.archlinux.org/index.php/KDE#KF5.2FQt5_applications_do_not_display_icons_in_i3.2Ffvwm.2Fawesome
set -x QT_QPA_PLATFORMTHEME qt5ct
#set -x QT_WAYLAND_DISABLE_WINDOWDECORATION 1
#set -x _JAVA_AWT_WM_NONREPARENTING 1
set -x LIBVA_DRIVER_NAME i965
