function clean # blog.ielliott.io/how-to-delete-old-nixos-boot-configurations
  sudo nix-env --delete-generations old --profile /nix/var/nix/profiles/system
  nix-store --gc
  nix-collect-garbage --delete-old
end
