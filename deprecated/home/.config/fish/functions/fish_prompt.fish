function fish_prompt
	# (whoami) (prompt_hostname) (string replace "/home/$USER" \~ $PWD)
	set_color purple
	printf $LOGNAME
	set_color red
	printf @
	set_color FF0
	printf (prompt_pwd)
	set_color green
	printf ' \u03BB ' # \xCE\xBB ☠ ➤
	set_color normal
end
