if &compatible
	set nocompatible " Be iMproved
endif

" dag/vim-fish#teach-a-vim-to-fish
" coderwall.com/p/lzf7ig/using-vim-with-fish-shell
"if &shell =~# 'fish$'
"	set shell=sh
"endif

" zchee/.nvimrc
if has('vim_starting')
	set runtimepath+=~/.config/nvim/repos/github.com/Shougo/dein.vim
endif

if (dein#load_state(expand('~/.config/nvim')))
	call dein#begin('~/.config/nvim')
	call dein#add('Shougo/dein.vim.git')

	" internal
	call dein#add('Shougo/deoplete.nvim')
	"call dein#add('equalsraf/neovim-gui-shim')
	if !has('nvim')
		call dein#add('roxma/nvim-yarp')
  		call dein#add('roxma/vim-hug-neovim-rpc')
	endif
	call dein#add('w0rp/ale')
	call dein#add('autozimu/LanguageClient-neovim', {
	\	'rev': 'next', 'build': './install.sh' }) " make release
	call dein#add('editorconfig/editorconfig-vim')

	" interface
	call dein#add('itchyny/lightline.vim')
	call dein#add('NLKNguyen/papercolor-theme')
	call dein#add('mhartington/oceanic-next')
	call dein#add('chriskempson/base16-vim')
	call dein#add('morhetz/gruvbox')
	call dein#add('dracula/vim')
	call dein#add('cocopon/iceberg.vim')

	call dein#add('stephpy/vim-yaml')
	call dein#add('cespare/vim-toml')
	call dein#add('jansenm/vim-cmake')
	call dein#add('richq/vim-cmake-completion')

	call dein#add('mboughaba/i3config.vim')
	call dein#add('LnL7/vim-nix')
	"call dein#add('MarcWeber/vim-addon-nix')
	"call dein#add('zah/nim.vim') " baabelfish/nvim-nim
	"call dein#add('ElmCast/elm-vim')
	"call dein#add('jakwings/vim-pony')
	"call dein#add('JuliaEditorSupport/julia-vim')

	" rust
	call dein#add('rust-lang/rust.vim')
	"call dein#add('racer-rust/vim-racer')
	"call dein#add('sebastianmarkow/deoplete-rust')

	" golang
	call dein#add('fatih/vim-go') " :GoInstallBinaries
	call dein#add('zchee/deoplete-go', {'build': 'make'})

	"call dein#add('Shougo/deoplete-clangx')
	"call dein#add('cquery-project/cquery')

	" gentoo/gentoo-syntax nfnty/vim-nftables

	call dein#end()
	call dein#save_state()
endif

if dein#check_install()
	call dein#install()
endif

let g:netrw_liststyle = 3
let g:autofmt_autosave = 1
let g:deoplete#enable_at_startup = 1
" github.com/Shougo/deoplete.nvim/issues/115#issuecomment-170237485
autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif
" Disable the candidates in Comment/String syntaxes.
call deoplete#custom#source('_',
             \ 'disabled_syntaxes', ['Comment', 'String'])

let g:go_fmt_command = 'goimports'

" LSP plugin
" https://afnan.io/2018-04-12/my-neovim-development-setup/
"let g:LanguageClient_devel = 1
"let g:LanguageClient_trace = 'verbose'
"let g:LanguageClient_loggingLevel = 'DEBUG'
"\	'cpp' : ['cquery', '--log-file=/tmp/cq.log'],
let g:LanguageClient_serverCommands = {
\	'cpp' : ['clangd'],
\	'rust': ['rustup', 'run', 'nightly', 'rls'] } " github.com/rust-lang-nursery/rls/issues/502#issuecomment-333100694
let g:LanguageClient_settingsPath = '/home/issam/.config/nvim/settings.json'

" Theming
let g:PaperColor_Theme_Options = { 'theme': {
\	'default': { 'transparent_background': 1 } } }
let g:PaperColor_Theme_Options = { 'language': {
\	'c': { 'highlight_builtins' : 1 },
\	'cpp': { 'highlight_standard_library': 1 },
\	'python': { 'highlight_builtins' : 1 } } }
"let g:dracula_colorterm = 0
"let base16colorspace=256  " Access colors present in 256 colorspace

" stackoverflow.com/q/2600783
cnoremap w!! w !sudo dd status=none of=%

" github.com/rust-lang/rust.vim/issues/108#issuecomment-292197030
filetype indent plugin on

" stackoverflow.com/a/33380495
if !exists('g:syntax_on')
	syntax enable
endif

" roxma/vim-hug-neovim-rpc#requirements
" vi.stackexchange.com/a/445
" Required for operations modifying multiple buffers like rename
"\	softtabstop=4
"\	shiftwidth=4
set
\	exrc
\	secure
\	hidden
\	tabstop=4
\	shiftwidth=4
\	noexpandtab
\	encoding=utf-8
\	colorcolumn=120
\	autoread
\	autowrite
\	number
\	nobackup
\	cursorline
\	noswapfile
\	visualbell
\	noerrorbells
"\	numberwidth=5

highlight ColorColumn ctermbg=red

" Map#0 (k_free & superuser.com/a/400502)
noremap ; l
noremap l j
noremap j h

" mharington/oceanic-next#installation
if has('termguicolors')
	set termguicolors
endif

set background=dark
colorscheme PaperColor
"hi Normal guibg=NONE ctermbg=NONE

" Map#1 (shift one keys’ position ; groove on J…)
"noremap ; l
"noremap l k
"noremap k j
"noremap j h

" Map#2 (stackoverflow.com/a/3084012)
"noremap ; l
"noremap l h
"noremap k j
"noremap j k

" github.com/altercation/vim-colors-solarized#important-note-for-terminal-users
"let g:solarized_termcolors=256

" github.com/tomasiser/vim-code-dark#the-background-color-in-my-terminal-is-wrong-when-there-is-no-text
" github.com/Microsoft/BashOnWindows/issues/1706#issuecomment-280522843
" github.com/NLKNguyen/papercolor-theme#installation
"set t_Co=256
"set t_ut=
"set term=screen-256color

"set background=dark
