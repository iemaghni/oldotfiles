{ config, lib, pkgs, ... }:
# https://matrix.org/_matrix/media/v1/download/matrix.org/SklPzNCoVJAceYEVZUrfaPix
# https://github.com/matejc/helper_scripts/blob/master/matej41/configuration.nix
# https://github.com/andsild/dotfiles/blob/master/nixos/core.nix
# https://github.com/NixOS/nixpkgs/issues/26511 CHROMIUM
#let
#  unstable = import <unstable> {};
#in
with pkgs;
{
  imports = [
  # ./desktop.nix
    ./laptop.nix
  ];
  boot = {
    loader = {
      grub.enable = false;
      systemd-boot.enable = true;      # gummiboot.enable
      efi.canTouchEfiVariables = true; # efi.efibootmgr.enable
    };
    kernel.sysctl = {
      "fs.inotify.max_user_watches" = 524288;
    };
    # https://github.com/NixOS/nixpkgs/issues/33414#issuecomment-355187253
    # https://github.com/NixOS/nixpkgs/issues/29409#issuecomment-329724677
    kernelPackages = linuxPackages_latest; # _hardened _copperhead_hardened
    # https://github.com/NixOS/nixpkgs/issues/26801#issuecomment-310942982
    extraModulePackages = [ config.boot.kernelPackages.wireguard ];
    supportedFilesystems = config.boot.initrd.supportedFilesystems;
  };
  hardware = {
  # parallels.enable
  # opengl.extraPackages = with pkgs; [ vaapiIntel libvdpau-va-gl vaapiVdpau ];
  # nvidiaOptimus.disable = true;
  # bumblebee = {
    # enable = true;
    # connectDisplay = true;
    # driver = "nouveau";
    # pmMethod = "bbswitch"; # 24711#issuecomment-357791636
  # };
    bluetooth = {
      enable = true;
      powerOnBoot = false;
    };
    enableRedistributableFirmware = true;
    cpu.intel.updateMicrocode = true;
    pulseaudio.enable = true;
  };
  system.autoUpgrade = {
    enable = true;
    # https://stackoverflow.com/a/37231521
    # hyper_ch: "or you could even use unstable small" on IRC
    # yes: https://nixos.org/nixos/manual/options.html#opt-system.autoUpgrade.channel
    # no : https://nixos.org/nixos/manual/index.html#idm140737316794256
    channel = https://nixos.org/channels/nixos-unstable-small;
  };
  nix = {
    #useSandbox = true; # useChroot
    maxJobs = lib.mkDefault 8;
  };
  networking.wireless = {
    enable = true;
    # nixpkgs/issues/{1204/9466}
  # userControlled.enable = true;
  };
  programs = {
    bash.enableCompletion = true;
    fish.enable = true;
    sway.enable = true;
    light.enable = true;
  };
  environment = {
    # 29137#issuecomment-354231105
    variables = {
      #JAVA_HOME = [ "${zulu8}" ]; # programs.java.enable
      GIO_EXTRA_MODULES = [ "${gvfs}/lib/gio/modules" ];
    };
    # 12705#issuecomment-177446823
    systemPackages = [ wireguard ntfs3g ];
  };
  # issues/31121
  services = {
    # https://github.com/NixOS/nixpkgs/issues/33540#issuecomment-363104193
    kresd = {
      enable = true;
      extraConfig = ''
        policy.add(policy.all(policy.TLS_FORWARD({
          { '2620:fe::fe', hostname = 'dns.quad9.net', ca_file = '/etc/ssl/certs/ca-bundle.crt' },
        })))
      '';
    };
    upower.enable = true;
  # fwupd.enable = true; # UMOCKDEV FAILING
  # autofs.enable = true;
    udisks2.enable = true;
  # gnome3.gvfs.enable = true;
  };
  i18n = {
    consoleKeyMap = "cf";
    defaultLocale = "en_CA.UTF-8";
    # POSIX glibcLocales supportedLocales
    # C.UTF-8 NixOS/nixpkgs/issues/20192
  };
  security = {
    #https://github.com/NixOS/nixos/issues/293#issuecomment-234194392
  # pam.services.<name?>.limits
  # lockKernelModules = true;
    sudo.wheelNeedsPassword = false;
    initialRootPassword = "$6$Io6u9MS3gg.oJL$ithppnltKUgS6pIzby3..257u43EK11HZDRvqejGpEgtu6PIjWHx6OJswDOCGBmg9iX537bzvMCMF000Fmazs0";
  };
  users = {
    motd = "Yo!";
    mutableUsers = false;
    defaultUserShell = bashInteractive;
    groups."www-data".gid = 82;
    extraUsers.issam = {
      uid = 1000;
      isNormalUser = true;
      description = "Issam Maghni";
      initialHashedPassword = config.users.users.root.hashedPassword; # NixOS/nixpkgs/issues/1752
      #"$6$Io6u9MS3gg.oJL$ithppnltKUgS6pIzby3..257u43EK11HZDRvqejGpEgtu6PIjWHx6OJswDOCGBmg9iX537bzvMCMF000Fmazs0";
      extraGroups = [ "wheel" "audio" "sway" ]; # "power"
      shell = fish;
      packages = with lxqt; [ # flatpak pipewire lightnvm sssd
      # alsaTools dbus_libs buck pasystray afuse fuse3 ponyc julia glances grub2_light gitMinimal st inetutils jwhois
      # qtpass qps lxqt-qtplugin audaciousQt5 otter-browser
      # i3status-rs: alsaUtils
      # jetbrains.jdk clang-tools
      # xclip https://github.com/neovim/neovim/issues/2889#issuecomment-251617809
      # NEOVIM_QT SHOULD SUPPORT FONT LIGATURE AND HANDLE PROPERLY COPY/PASTE
      # BY THEN, IT’S A NO-GO
        woeusb psmisc pciutils alsaUtils glxinfo lm_sensors hdparm smartmontools
        testssl dnsutils traceroute whois nmap mosh openvpn
        wget subversion git go rustup cargo lld_6 clang_6 jdk8 zulu cquery
        gnumake bazel cmake ninja binutils pkgconfig editorconfig-core-c
        qt5.qtwayland xclip visualvm
        file tree cv pv p7zip unzip nnn exa dfc fzf fd ripgrep screen gnupg nginx
        neofetch cowsay ponysay mediainfo imagemagick ffmpeg feh imlib2 pandoc
        neovim pass cmus mutt htop i3status-rust j4-dmenu-desktop
        wpa_supplicant_gui mumble_git sqlitebrowser kitty
        qterminal pavucontrol-qt pcmanfm-qt # lxqt
        xorg.xhost xorg.xrandr xorg.xprop xorg.xev xorg.xmodmap # xorg
        keepassx-community libreoffice-fresh wireshark
        jetbrains.idea-community eclipses.eclipse-sdk
        android-studio-preview androidsdk_extras
        kdeFrameworks.oxygen-icons5 gvfs gamin # PCManFM
        qutebrowser chromium falkon qpdfview qmmp
      ];
    };
  };
  fonts.fonts = [ fira-code font-awesome_5 ]; # extraFonts
  time.timeZone = "America/Montreal";
# sound.extraConfig = "defaults.pcm.!device 3";
# services.shellinabox.enable
}
