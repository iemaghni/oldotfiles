{ config, ... }:

{
  # dm-crypt
  boot = {
    kernelModules = [ "kvm-intel" ];
    initrd = {
      availableKernelModules = [ "ehci_pci" "ahci" "xhci_pci" "usbhid" "usb_storage" "sd_mod" "sr_mod" ];
      supportedFilesystems = [ "vfat" "xfs" ];
      luks = {
        cryptoModules = [ "aes" "aes_generic" "aes_x86_64" "xts" "sha256" ];
        devices."luks".device = "/dev/sda3";
      };
    };
  };
  fileSystems = [ {
    mountPoint = "/boot";
    device = "/dev/sda1";
  } {
    mountPoint = "/";
    device = "/dev/mapper/vg0-root";
  } {
    mountPoint = "/home";
    device = "/dev/mapper/vg0-home";
  } ];
  # https://github.com/NixOS/nixpkgs/issues/8277#issuecomment-234716206
  swapDevices = [ {
    device = "/dev/sda2";
    randomEncryption.enable = true; # Cannot hibernate
  # encrypted = {
  #   enable = true;
  #   blkDev = "/dev/sda2";
  # };
  } ];
  powerManagement.cpuFreqGovernor = "performance";
  nixpkgs.config.allowUnfree = true;
}
