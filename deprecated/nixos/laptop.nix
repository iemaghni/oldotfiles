{ config, lib, pkgs, ... }:
# https://szibele.com/gpu-passthrough-using-qemu-and-kvm
# https://wiki.debian.org/SSDOptimization
# https://sites.google.com/site/easylinuxtipsproject/ssd
# gist of domenkozar
# PHC PATCH
with lib;
{
  imports = [
  # ./vfio.nix
  # ./wg.nix
  ];
  boot = {
  # snd_pcm_oss
    # libata.force=2.00:disable askubuntu.com/a/387261
    kernelParams = [ "i915.enable_psr=1" ]; # extraKernelParams
    blacklistedKernelModules = [ "psmouse" ];
    initrd = {
      kernelModules = mkBefore [ "kvm-intel" ]; # extraKernelModules
      availableKernelModules = [ "dm-crypt" "xhci_pci" "ahci" "nvme" "usb_storage" "sd_mod" "snd_pcm_oss" ]; # "acpi_call"
      supportedFilesystems = [ "vfat" "xfs" ];
      luks = {
        cryptoModules = [ "aes" "aes_generic" "aes_x86_64" "xts" "sha256" ];
        devices."luks".device = "/dev/nvme0n1p3";
      };
    };
    kernel.sysctl = {
      "vm.swappiness" = 10;
      "vm.vfs_cache_pressure" = 50;
    };
  # plymouth.enable = true;
  };
  fileSystems = [ {
    mountPoint = "/boot";
    device = "/dev/nvme0n1p1";
  } {
    mountPoint = "/";
    device = "/dev/mapper/vg0-root";
  } {
    mountPoint = "/home";
    device = "/dev/mapper/vg0-home";
  } ];
  # https://github.com/NixOS/nixpkgs/issues/8277#issuecomment-234716206
  swapDevices = [ {
    device = "/dev/nvme0n1p2";
    randomEncryption.enable = true; # Cannot hibernate (bad for SSD’s longevity)
  } ];
  powerManagement = {
    enable = true;
    powertop.enable = true;
    cpuFreqGovernor = mkDefault "powersave"; # Clashes with TLP
    # https://unix.stackexchange.com/a/263633
    powerUpCommands = "${pkgs.hdparm}/sbin/hdparm -S1 /dev/sda"; # -YB{127,255}
  };
# hardware.bumblebee = {
  # enable = true; # DISABLE THIS IF VFIO
  # driver = "nouveau";
  # pmMethod = "bbswitch";
# };
  services = {
    # https://askubuntu.com/q/554398#comment760836_554398
    udev.extraRules = ''
      KERNEL=="sda1", ENV{UDISKS_IGNORE}="1"
    '';
    tlp.enable = true;
  };
# nixpkgs.config.allowUnfree = true;
  environment.systemPackages = [ pkgs.powertop ];
  virtualisation.libvirtd.enable = true;
  users.groups.libvirtd.members = [ "root" "issam" ];
}
