{ config, pkgs, ... }:
# 01:00.0 VGA compatible controller [0300]: NVIDIA Corporation GP107M [GeForce GTX 1050 Ti Mobile] [10de:1c8c] (rev a1)
# https://github.com/NixOS/nixpkgs/pull/33754
# https://github.com/georgewhewell/nixos-host/blob/master/services/virt/vfio.nix
# http://blog.matejc.com/blogs/myblog/playing-on-qemu
{
  boot = {
    kernelParams = [ "intel_iommu=on" "vfio_pci.ids=10de:1c8c" ];
    kernelModules = [ "kvm_intel" "vfio" "vfio_virqfd" "vfio_pci" "vfio_iommu_type1" ];
    blacklistedKernelModules = [ "nouveau" ]; # redd.it/5utaw9/ddwxat4
  # extraModprobeConfig
  };
  virtualisation.libvirtd = {
    enable = true; # enableKVM + qemuOvmf
    # gist.github.com/techhazard/1be07805081a4d7a51c527e452b87b26#gistcomment-2087168
    qemuVerbatimConfig = '' nvram = [ "${pkgs.OVMF}/FV/OVMF.fd:${pkgs.OVMF}/FV/OVMF_VARS.fd" ] '';
  };
  users.groups.libvirtd.members = [ "root" "issam" ]; # user[uid=1000]
  environment.systemPackages = with pkgs; [ OVMF qemu virtmanager ]; # virtmanager-qt libvirt-glib
}
