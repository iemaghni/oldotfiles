curl https://sh.rustup.rs -sSf | sh
rustup self update
rustup update nightly
rustup defualt nighlty
# https://github.com/rust-lang-nursery/rls/issues/502#issuecomment-333100694
# --toolchain nightly
rustup component add rls-preview
rustup component add rust-analysis
rustup component add rust-src
