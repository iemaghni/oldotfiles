sudo pacman -Syu --needed \
	fakeroot binutils pkg-config gcc lld make \
	wireguard-tools nftables \
	gvfs gamin pipewire upower flatpak \
	glfw-wayland qt5-wayland qt5-webkit \
	libx264-10bit xdg-desktop-portal-gtk \
	ttf-ubuntu-font-family otf-font-awesome oxygen-icons \
	neofetch lolcat doge ponysay editorconfig-core-c \
	rust go mosh mpv cmus neovim python-pip \
	qterminal qutebrowser keepassxc sylpheed \
	pcmanfm-qt transmission-qt baka-mplayer \
	android-file-transfer jdk9-openjdk \
	intellij-idea-community-edition \
	htop wget exa dfc fzf fd-rs ripgrep
