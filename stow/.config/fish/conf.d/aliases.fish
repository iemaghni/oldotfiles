alias sudoe sudo\ --preserve-env
alias xx exit
alias zz suspend
alias po 'poweroff;or sudo poweroff'
alias re 'reboot;or sudo reboot'
#alias ls 'ls --color --human-readable --group-directories-first'
alias mk make
alias mkc 'make clean'
alias mkd 'make debug'
alias mki 'make install'
alias cip 'curl icanhazip.com'
alias syu 'sudo pacman --sync --refresh --sysupgrade'
alias vkp 'sudo vkpurge rm all'
alias mpa 'mpv --no-video'
alias udf 'git -C ~/dotfiles pull'
alias key xev
alias keys 'xmodmap -pke'
alias nmutt neomutt
alias ciphers 'nmap --script ssl-enum-ciphers -p 443' # superuser.com/a/763908
alias wpa_cli  '/usr/bin/wpa_cli -iwlp3s0'
#alias gpg     '/usr/bin/gpg2 --homedir=~/.config/gnupg'
alias irssi    '/usr/bin/irssi --home=~/.config/irssi'
alias mbsync   '/usr/bin/mbsync --config ~/.config/mbsync/rc'
alias calcurse '/usr/bin/calcurse --directory ~/.config/calcurse'
