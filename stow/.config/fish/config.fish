# (go env GOPATH) (yarn global bin)/..
for _d in /opt/nginx /opt ~/go ~/.local ~/.gem/ruby/*
	test -d $_d; and set PATH $PATH $_d/bin
end

set fish_color_command white
set fish_color_autosuggestion brblack

set -x EDITOR nvim
set -x GNUPGHOME $XDG_CONFIG_HOME/gnupg
set -x LIBVA_DRIVER_NAME iHD # Intel Broadwell (5th-gen) and later
#set -x MESA_LOADER_DRIVER_OVERRIDE iris # Intel Skylake (6th-gen) and later
set -x QT_QPA_PLATFORMTHEME qt5ct # wiki.archlinux.org/index.php/KDE#KF5/Qt_5_applications_do_not_display_icons_in_i3/FVWM/awesome
set -x QT_QPA_PLATFORM wayland
set -x QT_WAYLAND_DISABLE_WINDOWDECORATION 1
set -x QT_WAYLAND_FORCE_DPI 96
set -x _JAVA_AWT_WM_NONREPARENTING 1
set -x QUTE_SKIP_WAYLAND_CHECK 1
set -x KITTY_ENABLE_WAYLAND 1
set -x SWAY_CURSOR_THEME Simple-and-Soft
set -x RANGER_LOAD_DEFAULT_RC FALSE
set -x MOZ_CRASHREPORTER_DISABLE 1

#set -q XDG_RUNTIME_DIR; or \
#set -x XDG_RUNTIME_DIR /run/user/(id -u)
