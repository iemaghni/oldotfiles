function osrel
	set -l IFS =
	while read name value
		set -g $name $value                         
	end < /etc/os-release
end
