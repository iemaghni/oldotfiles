function! ale_linters#c#clang_tidy#GetCommand(buffer) abort
	if expand('#' . a:buffer) =~# '\v\.h$'
		" -fix may clash with clang-format
		return '%e ' . expand('#' . a:buffer . ':r') . '.c -header-filter=' . expand('#' . a:buffer) . ' -p build'
	endif
    return '%e %s -p=build'
endfunction

call ale#linter#Define('c', {
\   'name': 'clang_tidy',
\   'output_stream': 'stdout',
\   'executable_callback': {b -> ale#Var(b, 'c_clangtidy_executable')},
\   'command_callback': function('ale_linters#c#clang_tidy#GetCommand'),
\   'callback': 'ale#handlers#gcc#HandleGCCFormat',
\   'lint_file': 1,
\})
