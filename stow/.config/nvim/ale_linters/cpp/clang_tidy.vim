function! ale_linters#cpp#clang_tidy#GetCommand(buffer) abort
	if expand('#' . a:buffer) =~# '\v\.hpp$'
		" -fix may clash with clang-format
		return '%e ' . expand('#' . a:buffer . ':r') . '.cpp -header-filter=' . expand('#' . a:buffer) . ' -p build'
	endif
    return '%e %s -p=build'
endfunction

call ale#linter#Define('cpp', {
\   'name': 'clang_tidy',
\   'output_stream': 'stdout',
\   'executable_callback': {b -> ale#Var(b, 'cpp_clangtidy_executable')},
\   'command_callback': function('ale_linters#cpp#clang_tidy#GetCommand'),
\   'callback': 'ale#handlers#gcc#HandleGCCFormat',
\   'lint_file': 1,
\})
