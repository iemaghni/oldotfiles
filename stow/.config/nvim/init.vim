if &compatible
	set nocompatible " Be iMproved
endif

" dag/vim-fish#teach-a-vim-to-fish
" coderwall.com/p/lzf7ig/using-vim-with-fish-shell
"if &shell =~# 'fish$'
"	set shell=sh
"endif

" zchee/.nvimrc
if has('vim_starting')
	" echo &runtimepath
	" set runtimepath?
	" set runtimepath^=another_path
	set runtimepath+=~/.config/nvim/repos/github.com/Shougo/dein.vim
endif

if (dein#load_state(expand('~/.config/nvim')))
	call dein#begin('~/.config/nvim')
	call dein#add('Shougo/dein.vim.git')

	" internal
	call dein#add('Shougo/deoplete.nvim')
	call dein#add('Shougo/neopairs.vim')
	"call dein#add('equalsraf/neovim-gui-shim')
	if !has('nvim')
		call dein#add('roxma/nvim-yarp')
  		call dein#add('roxma/vim-hug-neovim-rpc')
	endif
	call dein#add('powerman/vim-plugin-autosess')
	call dein#add('w0rp/ale')
	call dein#add('autozimu/LanguageClient-neovim', {
	\	'rev': 'next', 'build': 'make release' }) " ./install.sh
	call dein#add('editorconfig/editorconfig-vim')
	call dein#add('lervag/vimtex')

	" interface
	"call dein#add('ctrlpvim/ctrlp.vim')
	"call dein#add('itchyny/lightline.vim')
	call dein#add('ap/vim-buftabline')
	call dein#add('tpope/vim-fugitive')
	call dein#add('junegunn/goyo.vim')
	"call dein#add('arcticicestudio/nord-vim')
	call dein#add('NLKNguyen/papercolor-theme')
	call dein#add('mhartington/oceanic-next')
	call dein#add('chriskempson/base16-vim')
	call dein#add('morhetz/gruvbox')
	call dein#add('dracula/vim')
	call dein#add('cocopon/iceberg.vim')

	call dein#add('stephpy/vim-yaml')
	call dein#add('cespare/vim-toml')
	call dein#add('pboettch/vim-cmake-syntax')
	call dein#add('richq/vim-cmake-completion')
	call dein#add('mesonbuild/meson', {'rtp': 'data/syntax-highlighting/vim'})

	call dein#add('mboughaba/i3config.vim')
	call dein#add('LnL7/vim-nix')
	"call dein#add('MarcWeber/vim-addon-nix')
	"call dein#add('zah/nim.vim') " baabelfish/nvim-nim
	"call dein#add('ElmCast/elm-vim')
	"call dein#add('jakwings/vim-pony')
	"call dein#add('JuliaEditorSupport/julia-vim')

	" rust
	call dein#add('rust-lang/rust.vim')
	"call dein#add('racer-rust/vim-racer')
	"call dein#add('sebastianmarkow/deoplete-rust')

	" golang
	call dein#add('fatih/vim-go') " :GoInstallBinaries
	call dein#add('zchee/deoplete-go', {'build': 'make'})

	"call dein#add('Shougo/deoplete-clangx')
	"call dein#add('cquery-project/cquery')

	" gentoo/gentoo-syntax nfnty/vim-nftables

	call dein#add('dpelle/vim-Grammalecte')

	call dein#end()
	call dein#save_state()
endif

if dein#check_install()
	" TODO hide "hit enter to continue"
	call dein#install()
endif

" CONFIGURATION
if !has('nvim')
	set ttymouse=sgr
endif

" https://shapeshed.com/vim-netrw/
let g:netrw_banner = 0
"let g:netrw_browse_split = 4
let g:netrw_liststyle = 3
let g:netrw_winsize = 10
" https://github.com/tpope/vim-vinegar/issues/13#issuecomment-47133890
autocmd FileType netrw setl bufhidden=delete

let g:autofmt_autosave = 1

" $VIMRUNTIME/filetype.vim
let c_syntax_for_h = 1

let g:goyo_width = 82

let g:autosess_dir = stdpath('cache') . '/autosess'

let g:deoplete#enable_at_startup = 1
let g:neopairs#enable = 1
" github.com/Shougo/deoplete.nvim/issues/608#issuecomment-357564432
call deoplete#custom#source('_', 'converters', ['converter_auto_paren'])
" github.com/Shougo/deoplete.nvim/issues/115#issuecomment-170237485
autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif
" Disable the candidates in Comment/String syntaxes.
call deoplete#custom#source('_', 'disabled_syntaxes', ['Comment', 'String'])

" VIMTEX plugin
call deoplete#custom#var('omni', 'input_patterns', {'tex': g:vimtex#re#deoplete})
let g:vimtex_format_enabled = 1
let g:vimtex_view_method = 'zathura'
let g:vimtex_compiler_progname = 'nvr'

let g:go_fmt_command = 'goimports'

"let g:EditorConfig_core_mode = 'external_command'
"let g:EditorConfig_exec_path =
"let g:EditorConfig_verbose = 1

let g:ale_completion_enabled = 0
"let g:ale_linters_explicit = 1
let g:ale_fix_on_save = 1
let g:ale_linters = {
\	'c':   ['clangd', 'clang_tidy'],
\	'cpp': ['clangd', 'clang_tidy'] }
"\	'c':   ['ccls', 'clangd', 'clangtidy', 'cppcheck', 'cpplint', 'cquery', 'flawfinder'],
"\	'cpp': ['ccls', 'clangd', 'clangcheck', 'clangtidy', 'clazy', 'cppcheck', 'cpplint', 'cquery', 'flawfinder'] }
" c++: clangcheck is redundant (see stackoverflow.com/a/50237062)
"let g:ale_cpp_clangtidy_executable = 'fsadsdf'
let g:ale_fixers = { 'c': ['clang-format'], 'cpp': ['clang-format'] }
let g:c_build_dir = 'build'
"let g:c_build_dir_names = [g:c_build_dir]

" LSP plugin
" https://afnan.io/2018-04-12/my-neovim-development-setup/
"let g:LanguageClient_devel = 1
"let g:LanguageClient_trace = 'verbose'
"let g:LanguageClient_loggingLevel = 'DEBUG'
"\	'cpp' : ['cquery', '--log-file=/tmp/cq.log'],
let g:LanguageClient_serverCommands = {
\	'c' :   ['clangd', '-compile-commands-dir=' . getcwd() . '/build'],
\	'cpp' : ['clangd', '-compile-commands-dir=' . getcwd() . '/build'],
\	'rust': ['rustup', 'run', 'nightly', 'rls'] } " github.com/rust-lang-nursery/rls/issues/502#issuecomment-333100694
let g:LanguageClient_settingsPath = '/home/issam/.config/nvim/settings.json'
"let g:LanguageClient_diagnosticsEnable = 0

" Theming
" https://github.com/NLKNguyen/papercolor-theme/issues/83#issuecomment-281517318
let g:PaperColor_Theme_Options = {
\	'language': {
\		'c': { 'highlight_builtins' : 1 },
\		'cpp': { 'highlight_standard_library': 1 },
\		'python': { 'highlight_builtins' : 1 }
\	}, 'theme': { 'default.dark': { 
\			'transparent_background': 0,
\}	}	}
"\			'override' : { 'color00' : ['0'] }
"let g:dracula_colorterm = 0
"let base16colorspace=256  " Access colors present in 256 colorspace

" stackoverflow.com/q/2600783
cnoremap w!! w !sudo dd status=none of=%

" github.com/rust-lang/rust.vim/issues/108#issuecomment-292197030
filetype indent plugin on

" stackoverflow.com/a/33380495
if !exists('g:syntax_on')
	syntax enable
endif

" roxma/vim-hug-neovim-rpc#requirements
" vi.stackexchange.com/a/445
" Required for operations modifying multiple buffers like rename
"\	softtabstop=4
"\	shiftwidth=4
set
\	exrc
\	secure
\	hidden
\	tabstop=4
\	shiftwidth=4
\	noexpandtab
\	encoding=utf-8
\	colorcolumn=80
\	scrolloff=99
\	ignorecase
\	smartcase
\	autoread
\	autowrite
\	number
\	nobackup
\	cursorline
\	noswapfile
\	splitright
\	splitbelow
\	visualbell
\	noerrorbells
\	noruler
\	laststatus=0
\	ssop-=folds   " do not store folds (stackoverflow.com/a/1651061)
\	ssop-=options " do not store global and local values in a session
"\	numberwidth=5
"\	noshowmode ONLY WITH LIGHTLINE

highlight ColorColumn ctermbg=red

" mharington/oceanic-next#installation
if has('termguicolors')
	set termguicolors
endif

set background=dark
colorscheme PaperColor "OceanicNext dracula
"hi Normal guibg=NONE ctermbg=NONE

cnoreabbrev Q q!

" superuser.com/a/400502
noremap <C-h> <Nop>
noremap h <Nop>
noremap j h
"noremap k kzz
"noremap l jzz
noremap l j
noremap ; l
map <C-j> j

nnoremap J :bp<lf>
nnoremap K <PageUp>
nnoremap L <PageDown>
nnoremap ' :bn<lf>

inoremap <A-j> <Left>
inoremap <A-k> <Up>
inoremap <A-l> <Down>
inoremap <A-;> <Right>
inoremap <A-BS> <C-w>

"inoremap { {}<Left>
inoremap {<cr> {<lf>}<Esc>O

nnoremap <A-j> <C-W><C-H>
nnoremap <A-k> <C-W><C-K>
nnoremap <A-l> <C-W><C-J>
nnoremap <A-;> <C-W><C-L>

tnoremap <Esc> <C-\><C-n>
tnoremap <A-j> <Left>
tnoremap <A-k> <Up>
tnoremap <A-l> <Down>
tnoremap <A-;> <Right>

map <C-t>e       :tab drop 
map <C-t><up>    :tabr<lf>
map <C-t><down>  :tabl<lf>
map <C-t><left>  :tabp<lf>
map <C-t><right> :tabn<lf>

" Commenting blocks of code. (stackoverflow.com/a/1676672)
autocmd FileType lua                           let b:comment_leader = '--'
autocmd FileType c,cpp,java,scala              let b:comment_leader = '//'
autocmd FileType asm,sh,ruby,python,conf,fstab let b:comment_leader = '#'
autocmd FileType tex                           let b:comment_leader = '%'
autocmd FileType mail                          let b:comment_leader = '>'
autocmd FileType vim                           let b:comment_leader = '"'
noremap <silent> ,cc :<C-b>silent <C-e>s/^/<C-r>=escape(b:comment_leader,'\/')
\	<lf>/<lf>:noh<lf>
noremap <silent> ,cu :<C-b>silent <C-e>s/^\V<C-r>=escape(b:comment_leader,'\/')
\	<lf>//e<lf>:noh<lf>

" https://stackoverflow.com/a/26551079
" Zoom / Restore window.
function! s:ZoomToggle() abort
    if exists('t:zoomed') && t:zoomed
        execute t:zoom_winrestcmd
        let t:zoomed = 0
    else
        let t:zoom_winrestcmd = winrestcmd()
        resize
        vertical resize
        let t:zoomed = 1
    endif
endfunction
command! ZoomToggle call s:ZoomToggle()
nnoremap <silent> <A-o> :ZoomToggle<lf>

" github.com/altercation/vim-colors-solarized#important-note-for-terminal-users
"let g:solarized_termcolors=256

" github.com/tomasiser/vim-code-dark#the-background-color-in-my-terminal-is-wrong-when-there-is-no-text
" github.com/Microsoft/BashOnWindows/issues/1706#issuecomment-280522843
" github.com/NLKNguyen/papercolor-theme#installation
"set t_Co=256
"set t_ut=
"set term=screen-256color

"set background=dark

"https://llvm.org/svn/llvm-project/cfe/trunk/tools/clang-format/clang-format.py
autocmd BufWritePost *.c,*.cc,*.cxx,*.cpp,*.h,*.hpp exe 'py3f ' . stdpath('config') . '/clang-format.py'

function s:detect_format() " DetectFormat()
	exts = ['cc', 'cxx']
	if index(expand('%:e'), exts) > 0
		echo 'Oops'
	endif
endfunction
