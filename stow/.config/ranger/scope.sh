#!/bin/sh
set -o noclobber -o noglob -o nounset -o pipefail
IFS=$'\n'

# If the option `use_preview_script` is set to `true`,
# then this script will be called and its output will be displayed in ranger.
# ANSI color codes are supported.
# STDIN is disabled, so interactive scripts won't work properly

# code | meaning    | action of ranger
# -----+------------+-------------------------------------------
# 0    | success    | Display stdout as preview
# 1    | no preview | Display no preview at all
# 2    | plain text | Display the plain content of the file
# 3    | fix width  | Don't reload when width changes
# 4    | fix height | Don't reload when height changes
# 5    | fix both   | Don't ever reload
# 6    | image      | Display the image `$IMAGE_CACHE_PATH` points to as an image preview
# 7    | image      | Display the file directly as an image

FILE_PATH="${1}"         # Full path of the highlighted file
PV_WIDTH="${2}"          # Width of the preview pane (number of fitting characters)
PV_HEIGHT="${3}"         # Height of the preview pane (number of fitting characters)
IMAGE_CACHE_PATH="${4}"  # Full path that should be used to cache image preview
PV_IMAGE_ENABLED="${5}"  # 'True' if image previews are enabled, 'False' otherwise.

HIGHLIGHT_SIZE_MAX=262143  # 256KiB
HIGHLIGHT_TABWIDTH=8
HIGHLIGHT_STYLE='pablo'
PYGMENTIZE_STYLE='autumn'

handle_image() {
	case "$1" in
		image/svg+xml)
			convert "${FILE_PATH}" "${IMAGE_CACHE_PATH}" && exit 6
			exit 1;;
		image/*)
		#	local orientation="$( identify -format '%[EXIF:Orientation]\n' -- "${FILE_PATH}" )"
		#	# If orientation data is present and the image actually
		#	# needs rotating ("1" means no rotation)...
		#	if [[ -n "$orientation" && "$orientation" != 1 ]]; then
		#		# ...auto-rotate the image according to the EXIF data.
		#		convert -- "${FILE_PATH}" -auto-orient "${IMAGE_CACHE_PATH}" && exit 6
		#	fi
			exit 7;;
		video/*)
			ffmpegthumbnailer -i "${FILE_PATH}" -o "${IMAGE_CACHE_PATH}" -s 0 && exit 6
			exit 1;;
		application/pdf)
			pdftoppm -f 1 -l 1 \
					-scale-to-x 1920 \
					-scale-to-y -1 \
					-singlefile \
					-jpeg -tiffcompression jpeg \
					-- "${FILE_PATH}" "${IMAGE_CACHE_PATH%.*}" \
				&& exit 6
			exit 1;;
		*)	# Do nothing, just pass.
    esac
}

handle_mime() {
	case "$1" in
		text/* | */xml)
            # Syntax highlight
            if [[ "$( stat --printf='%s' -- "${FILE_PATH}" )" -gt "${HIGHLIGHT_SIZE_MAX}" ]]; then
                exit 2
            fi
            if [[ "$( tput colors )" -ge 256 ]]; then
                local pygmentize_format='terminal256'
                local highlight_format='xterm256'
            else
                local pygmentize_format='terminal'
                local highlight_format='ansi'
            fi
            highlight --replace-tabs="${HIGHLIGHT_TABWIDTH}" --out-format="${highlight_format}" \
                --style="${HIGHLIGHT_STYLE}" --force -- "${FILE_PATH}" && exit 5
            # pygmentize -f "${pygmentize_format}" -O "style=${PYGMENTIZE_STYLE}" -- "${FILE_PATH}" && exit 5
            exit 2;;
		image/*)
			img2txt --gamma=0.6 --width="${PV_WIDTH}" -- "${FILE_PATH}" && exit 4
			exit 1;;
		video/* | audio/*)
			mediainfo "${FILE_PATH}" && exit 5
			exit 1;;
		*)	# Do nothing, just pass.
	esac
}

handle_fallback() {
    echo '----- File Type Classification -----' && file --dereference --brief -- "${FILE_PATH}" && exit 5
    exit 1
}


MIMETYPE="$( file --dereference --brief --mime-type -- "${FILE_PATH}" )"
[ "${PV_IMAGE_ENABLED}" == 'True' ] && handle_image "${MIMETYPE}"
handle_mime "${MIMETYPE}"
handle_fallback

exit 1
