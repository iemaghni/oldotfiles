// github.com/pyllyukko/user.js/blob/master/user.js
// privacytools.io/#about_config

// APIs {{{
	//user_pref('beacon.enabled', false);
	user_pref('browser.send_pings', false);
	user_pref('browser.send_pings.require_same_host', true);
	user_pref('device.sensors.enabled', false);
	user_pref('dom.enable_resource_timing', false);
	user_pref('dom.event.clipboardevents.enabled', false);
	user_pref('dom.flyweb.enabled', false);
	user_pref('dom.gamepad.enabled', false);
	user_pref('dom.IntersectionObserver.enabled', false);
	user_pref('dom.telephony.enabled', false);
	user_pref('dom.vibrator.enabled', false);
	user_pref('dom.battery.enabled', false);
	user_pref('dom.vr.enabled', false);
	user_pref('mathml.disabled', true);
	//user_pref('media.webspeech.recognition.enable', false); BY DEFAULT
	user_pref('media.webspeech.synth.enabled', false);
	user_pref('webgl.disable-extensions', true);
	user_pref('webgl.disable-fail-if-major-performance-caveat', true);
	user_pref('webgl.disabled', true);
	user_pref('webgl.enable-debug-renderer-info', false);
	user_pref('webgl.min_capability_mode', true);
	// Clipboard {{{
		user_pref('clipboard.autocopy', false);
		user_pref('dom.allow_cut_copy', false);
		user_pref('dom.event.clipboardevents.enabled', false);
	// }}}
// }}}

// Localization {{{
	user_pref('browser.search.countryCode', 'US');
	user_pref('browser.search.region', 'US');
	user_pref('browser.search.geoip.url', '');
	user_pref('browser.search.geoSpecificDefaults', false);
	user_pref('geo.enabled', false);
	user_pref('intl.accept_languages', 'en-US, en');
	user_pref('intl.locale.matchOS', false);
	user_pref('javascript.user_us_english_locale', true);
// }}}

// Spoofing {{{
	user_pref('general.useragent.override', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0');
	user_pref('browser.startup.homepage_override.buildID', '20100101');
	user_pref('general.buildID.override', '20100101');
	//user_pref('general.appname.override', 'Netscape'); BY DEFAULT
	user_pref('general.appversion.override', '5.0 (Windows)');
	user_pref('general.oscpu.override', 'Windows NT 6.1');
	user_pref('general.platform.override', 'Win32');
// }}}

// Privacy {{{
	user_pref('browser.discovery.enabled', false);
	user_pref('browser.display.use_document_fonts', 0);
	user_pref('browser.safebrowsing.downloads.remote.enabled', false);
	user_pref('browser.safebrowsing.malware.enabled', true);
	user_pref('browser.safebrowsing.phishing.enabled', true);
	user_pref('browser.safebrowsing.downloads.remote.enabled', false);
	user_pref('browser.safebrowsing.downloads.remote.url', '');
	user_pref('browser.safebrowsing.provider.google.reportURL', '');
	user_pref('browser.safebrowsing.provider.google4.reportURL', '');
	user_pref('browser.safebrowsing.provider.google.reportMalwareMistakeURL', '');
	user_pref('browser.safebrowsing.provider.google.reportPhishMistakeURL', '');
	user_pref('browser.safebrowsing.provider.google4.reportMalwareMistakeURL', '');
	user_pref('browser.safebrowsing.provider.google4.reportPhishMistakeURL', '');
	user_pref('browser.safebrowsing.provider.google4.dataSharing.enabled', false);
	user_pref('browser.safebrowsing.provider.google4.dataSharingURL', '');
	user_pref('browser.safebrowsing.reportPhishURL', '');
	user_pref('extensions.blocklist.url', 'https://blocklists.settings.services.mozilla.com/v1/blocklist/3/%20/%20/');
	//user_pref('keyword.enabled', false);
	user_pref('media.peerconnection.ice.default_address_only', true);
	user_pref('middlemouse.contentLoadURL', false);
	user_pref('network.cookie.cookieBehavior', 1);
	user_pref('network.http.referer.trimmingPolicy', 2);
	user_pref('network.http.referer.XOriginPolicy', 2);
//
	user_pref('network.http.referer.XOriginTrimmingPolicy', 2);
	user_pref('network.IDN_show_punycode', true);
	user_pref('privacy.donottrackheader.enabled', true);
	user_pref('privacy.firstparty.isolate', true);
	// Will override user-agent, platform, resolution, etc.
	//user_pref('privacy.resistFingerprinting', true);
	user_pref('privacy.trackingprotection.enabled', true);
	user_pref('privacy.trackingprotection.pbmode.enabled', true);
	//user_pref('privacy.userContext.enabled', true); BY DEFAULT
	//user_pref('security.enterprise_roots.enabled', false); BY DEFAULT
// }}}

// Mozilla, thank you but no thank you {{{
	// Awesome newtab /s {{{
		user_pref('browser.newtabpage.activity-stream.asrouter.providers.cfr', '');
		user_pref('browser.newtabpage.activity-stream.asrouter.providers.onboarding', '');
		user_pref('browser.newtabpage.activity-stream.asrouter.providers.snippets', '');
		user_pref('browser.newtabpage.activity-stream.asrouter.userprefs.cfr', false);
		user_pref('browser.newtabpage.activity-stream.default.sites', '');
		user_pref('browser.newtabpage.activity-stream.disableSnippets', true);
		user_pref('browser.newtabpage.activity-stream.discoverystream.config', '');
		user_pref('browser.newtabpage.activity-stream.feeds.aboutpreferences', false);
		user_pref('browser.newtabpage.activity-stream.feeds.asrouterfeed', false);
		user_pref('browser.newtabpage.activity-stream.feeds.discoverystreamfeed', false);
		user_pref('browser.newtabpage.activity-stream.feeds.favicon', false);
		user_pref('browser.newtabpage.activity-stream.feeds.migration', false);
		user_pref('browser.newtabpage.activity-stream.feeds.newtabinit', false); // newtab
		user_pref('browser.newtabpage.activity-stream.feeds.places', false);
		user_pref('browser.newtabpage.activity-stream.feeds.prefs', false);
		user_pref('browser.newtabpage.activity-stream.feeds.section.highlights', false);
		user_pref('browser.newtabpage.activity-stream.feeds.section.topstories', false);
		user_pref('browser.newtabpage.activity-stream.feeds.section.topstories.options', '');
		user_pref('browser.newtabpage.activity-stream.feeds.sections', false);
		user_pref('browser.newtabpage.activity-stream.feeds.snippets', false);
		user_pref('browser.newtabpage.activity-stream.feeds.systemtick', false);
		user_pref('browser.newtabpage.activity-stream.feeds.telemetry', false);
		user_pref('browser.newtabpage.activity-stream.feeds.topsites', false);
		user_pref('browser.newtabpage.activity-stream.filterAdult', false);
		user_pref('browser.newtabpage.activity-stream.fxaccounts.endpoint', '');
		user_pref('browser.newtabpage.activity-stream.improvesearch.topSiteSearchShortcuts', false);
		user_pref('browser.newtabpage.activity-stream.improvesearch.topSiteSearchShortcuts.searchEngines', '');
		user_pref('browser.newtabpage.activity-stream.migrationExpired', true);
		user_pref('browser.newtabpage.activity-stream.pocketCta', '');
		user_pref('browser.newtabpage.activity-stream.prerender', false);
		user_pref('browser.newtabpage.activity-stream.sectionOrder', '');
		user_pref('browser.newtabpage.activity-stream.section.highlights.includeBookmarks', false);
		user_pref('browser.newtabpage.activity-stream.section.highlights.includeDownloads', false);
		user_pref('browser.newtabpage.activity-stream.section.highlights.includePocket', false);
		user_pref('browser.newtabpage.activity-stream.section.highlights.includeVisited', false);
		user_pref('browser.newtabpage.activity-stream.section.highlights.rows', 0);
		user_pref('browser.newtabpage.activity-stream.section.topstories.rows', 0);
		user_pref('browser.newtabpage.activity-stream.showSearch', false);
		user_pref('browser.newtabpage.activity-stream.showSponsored', false);
		user_pref('browser.newtabpage.activity-stream.telemetry', false);
		user_pref('browser.newtabpage.activity-stream.telemetry.ping.endpoint', '');
		user_pref('browser.newtabpage.activity-stream.telemetry.ut.events', false); // BY DEFAULT
		user_pref('browser.newtabpage.activity-stream.topSitesRows', 0);
		user_pref('browser.newtabpage.directory.source', 'data:text/plain,');
		user_pref('browser.newtabpage.enabled', false);
		user_pref('browser.newtabpage.enhanced', false);
		user_pref('browser.newtabpage.introShown', true);
	// }}}
	user_pref('browser.ping-centre.telemetry', false);
	user_pref('datareporting.healthreport.uploadEnabled', false);
	user_pref('datareporting.healthreport.service.enabled', false);
	user_pref('datareporting.policy.dataSubmissionEnabled', false);
	user_pref('identity.fxaccounts.enabled', false);
	user_pref('loop.logDomains', false);
	user_pref('toolkit.telemetry.enabled', false);
	// Extensions {{{
		user_pref('browser.pocket.enabled', false);
		user_pref('extensions.pocket.enabled', false);
		// extensions.screenshots.system-disabled
		user_pref('extensions.screenshots.disabled', true);
		user_pref('extensions.screenshots.upload-disabled', true);
		user_pref('extensions.systemAddon.update.enabled', false);
		user_pref('extensions.systemAddon.update.url', '');
	// }}}
	// Shiled {{{
		user_pref('app.normandy.api_url', '');
		user_pref('app.normandy.enabled', false);
		user_pref('app.shield.optoutstudies.enabled', false);
		user_pref('extensions.shield-recipe-client.api_url', '');
		user_pref('extensions.shield-recipe-client.enabled', false);
	// }}}
// }}}

// Fine-tuning {{{
	user_pref('dom.webgpu.enable', true);
	user_pref('general.smoothScroll.msdPhysics.enabled', true);
	user_pref('gfx.canvas.azure.accelerated', true);
	//user_pref('gfx.webrender.all', true); ISSUES WITH WAYLAND
	//user_pref('gfx.webrender.enabled', true);
	user_pref('javascript.options.asyncstack', false);
	//user_pref('layers.acceleration.force-enabled', true);
	user_pref('pdfjs.enableWebGL', true);
// }}}

// Theming {{{
	// https://old.reddit.com/r/firefox/comments/bcph6f
	//user_pref('browser.fullscreen.autohide', false);
	user_pref('browser.in-content.dark-mode', true);
	user_pref('browser.uidensity', 1);
	user_pref('devtools.theme', 'dark');
	//lightweightThemes.selectedThemeID
	user_pref('extensions.activeThemeID', 'firefox-compact-dark@mozilla.org');
	user_pref('extensions.webextensions.themes.icons.enabled', true);
	user_pref('reader.color_scheme', 'dark');
	user_pref('toolkit.cosmeticAnimations.enabled', false);
	user_pref('ui.systemUsesDarkTheme', 1);
	user_pref('widget.chrome.allow-gtk-dark-theme', true);
	user_pref('widget.content.allow-gtk-dark-theme', true);
// }}}

// Misc {{{
	user_pref('accessibility.force_disabled', 1);
	// general.warnOnAboutConfig
	user_pref('browser.aboutConfig.showWarning', false);
	//user_pref('browser.download.forbid_open_with', true);
	user_pref('browser.download.hide_plugins_without_extensions', false);
	user_pref('browser.download.manager.addToRecentDocs', false);
	user_pref('browser.download.useDownloadDir', false);
	user_pref('browser.link.open_newwindow', 3);
	user_pref('browser.link.open_newwindow.restriction', 0);
	user_pref('browser.sessionstore.max_tabs_undo', 1);
	user_pref('browser.startup.homepage', 'https://old.reddit.com');
	// browser.defaultbrowser.notificationbar
	// browser.shell.didSkipDefaultBrowserCheckOnFirstRun
	// browser.startup.firstrunSkipsHomepage
	user_pref('browser.shell.checkDefaultBrowser', false);
	//user_pref('browser.tabs.drawInTitlebar', true);
	// browser.warnOnQuit browser.tabs.warnOnCloseOtherTabs
	user_pref('browser.tabs.warnOnClose', false);
	user_pref('browser.urlbar.suggest.searches', false);
	user_pref('browser.urlbar.trimURLs', false);
	user_pref('devtools.cache.disabled', true);
	user_pref('dom.disable_window_move_resize', true);
	user_pref('dom.popup_allowed_events', 'click dblclick');
	user_pref('dom.popup_maximum', 3);
	user_pref('dom.webnotifications.enabled', false);
	// general.useragent.compatMode.firefox (conkeror.org/UserAgent)
	user_pref('network.http.redirection-limit', 10);
	//user_pref('network.protocol-handler.external.ms-windows-store', false);
	user_pref('pdfjs.disabled', true);
	user_pref('privacy.sanitize.sanitizeOnShutdown', true);
	user_pref('security.dialog_enable_delay', 0);
	user_pref('signon.rememberSignons', false);
// }}}

// vim:foldmethod=marker
